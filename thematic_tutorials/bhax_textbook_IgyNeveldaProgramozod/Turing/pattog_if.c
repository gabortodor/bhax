#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int check(int *z,int *zcurr,int *zmax)
{
    *z = *z + *zcurr;
    if ( *z>=*zmax-1 ) 
        *zcurr = *zcurr * -1;
    if ( *z<=0 ) 
        *zcurr = *zcurr * -1; 
}

int main(void)
{
    WINDOW *window;
    window = initscr ();

    int x = 0;
    int y = 0;

    int xcurr = 1;
    int ycurr = 1;

    int xmax;
    int ymax;

    for ( ;; ) 
    {
        getmaxyx (window,ymax,xmax);
        mvprintw (y, x, "O");
        refresh ();
        usleep (100000);
        check(&x,&xcurr,&xmax);
        check(&y,&ycurr,&ymax);
    }
    return 0;
}
