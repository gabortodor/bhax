#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int check(int *z,int *zcurr,int array[])
{
    *z = *z + *zcurr;
    *zcurr = *zcurr * array[*z];
    
}
int main(void)
{
    WINDOW *window;
    window = initscr ();

    int x = 0;
    int y = 0;

    int xcurr = 1;
    int ycurr = 1;

    int xmax;
    int ymax;

    

    for ( ;; ) 
    {   
        getmaxyx (window,ymax,xmax);
        int w[xmax];
        int h[ymax];
        w[0]=-1;
        h[0]=-1;
        for(int i=1;i<xmax-1;i++)
            w[i]=1;
        for(int i=1;i<ymax-1;i++)
            h[i]=1;
        w[xmax-1]=-1;
        h[ymax-1]=-1;
        mvprintw (y, x, "O");
        refresh ();
        usleep (100000);
        check(&x,&xcurr,w);
        check(&y,&ycurr,h);
    }
    return 0;
}
