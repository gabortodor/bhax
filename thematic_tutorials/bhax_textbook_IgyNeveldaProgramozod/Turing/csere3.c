#include <stdio.h>
int main()
{
    int a=2430;
    int b=288;
    int c;
    
    printf("Alap:\na=%d\nb=%d\n",a,b);

    c=a;
    a=b;
    b=c;
    printf("\nCsere után:\na=%d\nb=%d\n",a,b);

    c=a;
    a=b;
    b=c;
    printf("\nVisszacsere után:\na=%d\nb=%d\n",a,b);
    return 0;
}