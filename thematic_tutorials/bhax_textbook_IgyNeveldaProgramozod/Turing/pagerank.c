#include  <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    int i,j;
    double Link[4][4]={
        {0.0, 0.0, 1.0 / 3.0, 0.0},
        {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
        {0.0, 1.0 / 2.0, 0.0, 0.0},
        {0.0, 0.0, 1.0 / 3.0, 0.0}
    };
    double PR[]={0.0,0.0,0.0,0.0};
    double PRV[]={1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};

    for(int k=0;k<100;k++)
    {
        for(i=0;i<4;i++)
        {
            PR[i]=0.0;
            for(j=0;j<4;j++)
                PR[i]+=(Link[i][j]*PRV[j]);
        }

        for(i=0;i<4;i++)
            PRV[i]=PR[i];
        
    }
    for(int i=0;i<4;i++)
        printf("%d.oldal PageRankja:%f\n",i+1,PR[i]);
    
    return 0;

}