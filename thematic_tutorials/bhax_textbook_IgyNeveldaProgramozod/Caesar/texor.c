#define MAX_TITKOS 4096
#define OLVASAS_BUFFER 256
#define KULCS_MERET 5  //megadjuk a kulcs hosszát
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>

double atlagos_szohossz (const char *titkos, int titkos_meret)
{
  int sz = 0;
  for (int i = 0; i < titkos_meret; ++i)
    if (titkos[i] == ' ')
      ++sz;

  return (double) titkos_meret / sz;
}

int tiszta_lehet (const char *titkos, int titkos_meret)
{
  // a tiszta szöveg valószínüleg tartalmazza a gyakori magyar szavakat
  // illetve az átlagos szóhossz vizsgálatával csökkentjük a
  // potenciális töréseket

  double szohossz = atlagos_szohossz (titkos, titkos_meret);

  return szohossz > 6.0 && szohossz < 9.0
    && strcasestr (titkos, "hogy") && strcasestr (titkos, "nem")
    && strcasestr (titkos, "az") && strcasestr (titkos, "ha");

}

void exor (const char kulcs[], int kulcs_meret, char titkos[], int titkos_meret)
{

  int kulcs_index = 0;

  for (int i = 0; i < titkos_meret; ++i)
    {

      titkos[i] = titkos[i] ^ kulcs[kulcs_index];
      kulcs_index = (kulcs_index + 1) % kulcs_meret;

    }

}

int exor_tores (const char kulcs[], int kulcs_meret, char titkos[],int titkos_meret)
{

  exor (kulcs, kulcs_meret, titkos, titkos_meret);

  return tiszta_lehet (titkos, titkos_meret);

}

int main (void)
{

  char kulcs[KULCS_MERET];
  char titkos[MAX_TITKOS];
  char *p = titkos;
  int olvasott_bajtok;
  char kulcslista[5]={'p','n','s','u','r'}; //Megadjuk, milyen karakterek szerepelnek a kulcsban

  // titkos fájl berántása
  while ((olvasott_bajtok =
	  read (0, (void *) p,
		(p - titkos + OLVASAS_BUFFER <
		 MAX_TITKOS) ? OLVASAS_BUFFER : titkos + MAX_TITKOS - p)))
    p += olvasott_bajtok;

  // maradék hely nullázása a titkos bufferben  
  for (int i = 0; i < MAX_TITKOS - (p - titkos); ++i)
    titkos[p - titkos + i] = '\0';

  // összes kulcs előállitása
  for (int ii = 0; ii <= KULCS_MERET-1; ++ii)
    for (int ji = 0; ji <= KULCS_MERET-1; ++ji)
      for (int ki = 0; ki <= KULCS_MERET-1; ++ki)
	for (int li = 0; li <= KULCS_MERET-1; ++li)
	  for (int mi = 0; mi <= KULCS_MERET-1; ++mi)
	  
		    kulcs[0] = kulcslista[ii];
		    kulcs[1] = kulcslista[ji];
		    kulcs[2] = kulcslista[ki];
		    kulcs[3] = kulcslista[li];
		    kulcs[4] = kulcslista[mi];

		    if (exor_tores (kulcs, KULCS_MERET, titkos, p - titkos))
		      printf
			("Kulcs: [%c%c%c%c%c]\nTiszta szoveg: [%s]\n",
			 kulcslista[ii], kulcslista[ji], kulcslista[ki], kulcslista[li],kulcslista[mi], titkos);

		    // újra EXOR-ozunk, így nem kell egy második buffer  
		    exor (kulcs, KULCS_MERET, titkos, p - titkos);
		  }

  return 0;
}
