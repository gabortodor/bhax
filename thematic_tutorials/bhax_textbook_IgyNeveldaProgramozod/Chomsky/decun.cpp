#include <iostream>

using namespace std;

int main(void)
{
    cout<<"Adja meg az átváltandó decimális számot:\n";
    int dec;
    string un;
    cin>>dec;
    
    for(int i=0;i<dec;i++)
        un+="|";
        
    cout<<"A megadott szám unáris változata: "<<un<<"\n";
    return 0;
}