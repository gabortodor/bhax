# include <stdio.h>

int* sum(int array[])
{
    int c=0;
    for(int i=0;i<5;i++)
        c+=array[i];
    int* cptr=&c;
    return cptr;

}


int main()
{
//1. Egész bevezetése
int a=10;
printf("1. \t%d\n",a);

//2. Egészre mutató mutató bevezetése
int* aptr=&a;
printf("2. \t%p\n",aptr);

//3. Egész referenciájának bevezetése
int&     aref=a;
printf("3. \t%d\n",aref);

//4. Egészek tömbjének bevezetése
int array[5]={1,2,3,4,5};
printf("4.\t");
for(int i=0;i<5;i++)
    printf("%d ",array[i]);
printf("\n");   

//5. Egészek tömbjének referenciájának bevezetése
int (&arrayref) [5]=array;
printf("5.\t");
for(int i=0;i<5;i++)
    printf("%d ",arrayref[i]);
printf("\n");
//6. Egészre mutató mutatók tömbjének bevezetése
int (*arrayptr) [5];
arrayptr=&array;
printf("6.\t");
for(int i=0;i<5;i++)
    printf("%p ",arrayptr[i]);
printf("\n");

//7. Egészre mutató mutatót visszaadó függvény bevezetése
printf("7.\t%p\n",sum(array));

//8.
//int (*fptr)(int) = &sum;
//printf("8.\t%p\n",fptr);

//9.
int b=8;

//10.


}