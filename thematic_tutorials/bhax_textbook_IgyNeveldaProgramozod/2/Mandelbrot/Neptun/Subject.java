import java.util.ArrayList;


public class Subject {
    private String name;
    private ArrayList<NeptunUser> students;
    private ArrayList<Class> classes;


    public Subject(AvailableSubjects subName) {
        this.name = subName.name();
        students=new ArrayList<>();
        classes=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void add(NeptunUser student){
        students.add(student);
    }
    public void remove(NeptunUser student){
        if(students.contains(student))
            students.remove(student);
    }
    public String getStudentsString(){
        String studs;
        if(students.size()<=0)
            return "There is no registered student for this subject!";
        else
        studs="Registered students for "+this.getName()+": ";
        for(NeptunUser stud:students){
            studs+=stud.getNeptunCode()+", ";
        }
        return studs;
    }

    public Class createClass(DaysOfWeek day,int hour,int l){
        Class c=new Class(day,hour,l);
        classes.add(c);
        return c;

    }

    public ArrayList<Class> getClasses() { return classes;}

    public class Class{
        private String day;
        private int startingHour;
        private int endingHour;
        private int limit;
        private int counter;
        private ArrayList<NeptunUser> studentsInClass;

        public Class(DaysOfWeek day,int hour,int l){
            this.day=day.name();
            startingHour=hour;
            endingHour=hour+2;
            limit=l;
            counter=0;
            studentsInClass=new ArrayList<>();
        }
        public Subject outer() {
            return Subject.this;
        }
        public void addToClass(NeptunUser student){
            counter++;
            studentsInClass.add(student);
        }
        public void removeFromClass(NeptunUser student){
            counter--;
            if(studentsInClass.contains(student))
                studentsInClass.remove(student);
        }
        public String getStudentsClassString(){
            String studs;
            if(studentsInClass.size()<=0)
                return "There is no registered student for this class!";
            else
            studs="Registered students for "+this.outer().getName()+" with the starting time of "+this.day+" "+this.startingHour+": ";
            for(NeptunUser stud:studentsInClass){
                studs+=stud.getNeptunCode()+", ";
            }
            return studs;
        }

        public int getStartingHour() { return startingHour; }

        public String getDay() { return day; }
        public int getLimit() { return limit; }
        public int getCounter() { return counter; }
    }
}
