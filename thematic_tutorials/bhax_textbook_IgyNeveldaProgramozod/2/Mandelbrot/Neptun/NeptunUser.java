import java.util.ArrayList;

public class NeptunUser{

    private String neptunCode;
    private ArrayList<Subject> registeredSubjects;

    public NeptunUser(Students student) {
        this.neptunCode = student.name();
        registeredSubjects=new ArrayList<>();
    }

    public String getNeptunCode() {
        return neptunCode;
    }

    public String  registerForSubject(Subject subject,int hour,String day){
        Subject.Class cls=Neptun.getSubjectClass(subject.getName(),hour,day);
        if(subject==null)
            return "Registration failed, because the subject you entered is invalid! Neptun code: "+this.getNeptunCode()+", subject: "+subject.getName();
        if(cls.getCounter()>=cls.getLimit())
            return "Registration failed, because the class limit was reached!";
        if(Neptun.getRegistrationPeriod()&&!registeredSubjects.contains(subject)) {
            registeredSubjects.add(subject);
            cls.addToClass(this);
            subject.add(this);
            return "Successful registration! Neptun code: "+this.getNeptunCode()+", subject name: "+subject.getName()+", starting time:"+cls.getDay()+" at  "+hour+" hours."+cls.getLimit()+"/"+cls.getCounter();
        }
        else
            return "Registration failed, because registration period has ended or you already registered for this subject! Neptun code: "+this.getNeptunCode()+", subject name: "+subject.getName();

    }

    public String  unregisterFromSubject(Subject subject,int hour,String day){
        Subject.Class cls=Neptun.getSubjectClass(subject.getName(),hour,day);
        if(Neptun.getRegistrationPeriod()&&registeredSubjects.contains(subject)) {
            if(registeredSubjects.contains(subject))
                registeredSubjects.remove(subject);
            cls.removeFromClass(this);
            subject.remove(this);
            return "Successful unregistration! Neptun code: "+this.getNeptunCode()+", subject name: "+subject.getName()+", starting time:"+cls.getDay()+" at  "+hour+" hours."+cls.getLimit()+"/"+cls.getCounter();
        }
        else
            return "unregistration failed, because registration period has ended or you already unregistered from this subject! Neptun code: "+this.getNeptunCode()+", subject name: "+subject.getName();

    }

    public String getRegisteredSubjectsString(){
        String subs;
        if(registeredSubjects.size()<=0)
            return "There is no registered subjects!";
        else
            subs=this.getNeptunCode()+"'s subjects: ";
            for(Subject sub:registeredSubjects){
                subs+=sub.getName()+", ";
            }
            return subs;
    }
}
