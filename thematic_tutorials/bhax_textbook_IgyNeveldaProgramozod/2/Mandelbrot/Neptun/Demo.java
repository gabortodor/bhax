import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        Neptun.setRegistrationPeriod(true);
        Neptun.createSubjects();
        Neptun.createStudents();

        Scanner sc=new Scanner(System.in);
        String command;
        System.out.println("Available commands:\nregister --> Registers a student for a specific class\nunregister --> Unregisters a student from a specific class\nsubjects --> Lists the registered subject for a specific student\ns_students --> Lists registered students for a specific subject\nc_students --> Lists registered students for a specific class\nquit --> Quits the program");

        mainLoop:while(true) {
            System.out.println("\nCommand:");
            command = sc.nextLine();
            switch(command) {
                case "register":
                    System.out.println("Please enter your Neptun code:");
                    String code=sc.nextLine();
                    System.out.println("Enter the name of the subject:");
                    String sub=sc.nextLine();
                    System.out.println("Enter the day:");
                    String day=sc.nextLine();
                    System.out.println("Finally the starting hour:");
                    int hour=Integer.parseInt(sc.nextLine());;
                    System.out.println(Neptun.getStudent(code).registerForSubject(Neptun.getSubject(sub),hour,day));
                    break;
                case "unregister":
                        System.out.println("Please enter your Neptun code:");
                        String code_u=sc.nextLine();
                        System.out.println("Enter the name of the subject:");
                        String sub_u=sc.nextLine();
                        System.out.println("Enter the day:");
                        String day_u=sc.nextLine();
                        System.out.println("Finally the starting hour:");
                        int hour_u=Integer.parseInt(sc.nextLine());;
                        System.out.println(Neptun.getStudent(code_u).unregisterFromSubject(Neptun.getSubject(sub_u),hour_u,day_u));
                        break;
                case "subjects":
                    System.out.println("Please enter your Neptun code:");
                    String code_rs=sc.nextLine();
                    System.out.println(Neptun.getStudent(code_rs).getRegisteredSubjectsString());
                    break;
                case "s_students":
                    System.out.println("Enter the name of the subject:");
                    String sub_ss=sc.nextLine();
                    System.out.println(Neptun.getSubject(sub_ss).getStudentsString());
                    break;
                case "c_students":
                    System.out.println("Enter the name of the subject:");
                    String sub_cs=sc.nextLine();
                    System.out.println("Enter the day:");
                    String day_cs=sc.nextLine();
                    System.out.println("Finally the starting hour:");
                    int hour_cs=Integer.parseInt(sc.nextLine());
                    System.out.println(Neptun.getSubjectClass(sub_cs,hour_cs,day_cs).getStudentsClassString());
                    break;
                case "quit":
                    break mainLoop;
                default:
                    System.out.println("Unknown command");
                    break;
            }
        }
    }
}
