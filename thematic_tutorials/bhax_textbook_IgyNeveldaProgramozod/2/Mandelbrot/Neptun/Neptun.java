import java.util.ArrayList;

abstract class Neptun {
    private static Boolean registrationPeriod;
    private static  ArrayList<Subject> subjects;
    private static  ArrayList<NeptunUser> students;
    private static  ArrayList<Subject.Class> classes;

    public static Boolean getRegistrationPeriod() {

        return registrationPeriod;
    }

    public static void setRegistrationPeriod(Boolean rp) {
        registrationPeriod = rp;
    }

    public static void createSubjects(){
        subjects=new ArrayList<>();
        classes=new ArrayList<>();
        int[] times={8,10,12,14,16};
        for(AvailableSubjects sub:AvailableSubjects.values()){
            Subject a=new Subject(sub);
            subjects.add(a);
            for(DaysOfWeek d: DaysOfWeek.values()) {
                for (int t : times)
                    classes.add(a.createClass(d,t, 20));
            }
        }
    }

    public static Subject getSubject(String subName){
        for(Subject sub: subjects){
            if(sub.getName().equalsIgnoreCase(subName))
                return sub;
        }
                System.out.println("Not a valid subject!");
                return null;
    }
    public static Subject.Class getSubjectClass(String subName, int hour,String day){
        Subject a=Neptun.getSubject(subName);
        for(Subject.Class c:a.getClasses()){
            if(c.getStartingHour()==hour && c.getDay().equalsIgnoreCase(day))
                return c;
        }
        System.out.println("Not a valid class!");
        return null;
    }

    public static void createStudents(){
        students=new ArrayList<>();
        for(Students stud:Students.values()){
            students.add(new NeptunUser(stud));
        }
    }

    public static NeptunUser getStudent(String code){
        for(NeptunUser stud: students){
            if(stud.getNeptunCode().equalsIgnoreCase(code))
                return stud;
        }
        System.out.println("Not a valid neptun code");
        return null;
    }

}
