import java.util.ArrayList;

public class Subject {
	private String _name;
	private java.util.Vector<NeptunUser> _students;
	private java.util.ArrayList<Class> _classes;
	private java.util.Vector<Subject.Class> _attribute;

	public Subject(AvailableSubjects aSubname, AvailableSubjects aSubName) {
		throw new UnsupportedOperationException();
	}

	public void add(NeptunUser aStudent) {
		throw new UnsupportedOperationException();
	}

	public String getStudentsString() {
		throw new UnsupportedOperationException();
	}

	public void remove(NeptunUser aStudent) {
		throw new UnsupportedOperationException();
	}

	public Subject.Class createClass(DaysOfWeek aDay, int aHour, int aL) {
		throw new UnsupportedOperationException();
	}

	public Subject(AvailableSubjects aSubName) {
		throw new UnsupportedOperationException();
	}

	public String getName() {
		return this._name;
	}

	public java.util.ArrayList<Class> getClasses() {
		return this._classes;
	}

	public Subject.Class getAttribute() {
		return this._attribute;
	}
	public class Class {
		private String _day;
		private int _startingHour;
		private int _endingHour;
		private int _limit;
		private int _counter;
		private java.util.Vector<NeptunUser> _studentsInClass;

		public Class(DaysOfWeek aDay, int aHour, int aL) {
			throw new UnsupportedOperationException();
		}

		public Subject outer() {
			throw new UnsupportedOperationException();
		}

		public void addToClass(NeptunUser aStudent) {
			throw new UnsupportedOperationException();
		}

		public void removeFromClass(NeptunUser aStudent) {
			throw new UnsupportedOperationException();
		}

		public String getStudentsClassString() {
			throw new UnsupportedOperationException();
		}

		public String getDay() {
			return this._day;
		}

		public int getStartingHour() {
			return this._startingHour;
		}

		public int getLimit() {
			return this._limit;
		}

		public int getCounter() {
			return this._counter;
		}
	}
}