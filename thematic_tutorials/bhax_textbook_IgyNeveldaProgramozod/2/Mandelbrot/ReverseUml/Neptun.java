abstract class Neptun {
	private static java.util.Vector<Subject> _subjects;
	private static java.util.Vector<NeptunUser> _students;
	private static Boolean _registrationPeriod;
	private static java.util.Vector<Subject.Class> _classes;

	public static void createSubjects() {
		throw new UnsupportedOperationException();
	}

	public static Subject getSubject(String aSubName) {
		throw new UnsupportedOperationException();
	}

	public static void createStudents() {
		throw new UnsupportedOperationException();
	}

	public static NeptunUser getStudent(String aCode) {
		throw new UnsupportedOperationException();
	}

	public static Subject.Class getSubjectClass(String aSubName, int aHour, String aDay) {
		throw new UnsupportedOperationException();
	}

	public void setRegistrationPeriod(Boolean aRegistrationPeriod) {
		this._registrationPeriod = aRegistrationPeriod;
	}

	public Boolean getRegistrationPeriod() {
		return this._registrationPeriod;
	}
}