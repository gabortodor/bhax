import java.util.*;
import java.util.function.BiPredicate;

public class HashMap<K,V> implements java.util.Map<K,V>{

    private static final int SIZE=10;

    private int size = 0;
    private K[] keys = (K[]) new Object[SIZE];
    private V[] values = (V[]) new Object[SIZE];

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size<=0;
    }

    @Override
    public boolean containsKey(Object key) {
        if(size<=0)
            return false;
        else
           return search(key,keys,Objects::equals)>=0;
    }

    @Override
    public boolean containsValue(Object value) {
        int index;
        if(size<=0)
            return false;
        else
            index=search(value,values,Objects::equals);
        return index>-1 && keys[index]!=null;
    }

    @Override
    public V get(Object key) {
        Objects.requireNonNull(key);
        if(size <= 0)
            return null;
        int index=search(key,keys,Objects::equals);
        if(index>=0)
            return values[index];
        return null;
    }

    @Override
    public V put(K key, V value) {

        Objects.requireNonNull(key);

        int index = search(key, keys, Objects::equals);
        if (index < 0) {
            index = findEmpty();
            if (index < 0)
                expand();
            index = size;
        }

        V prevValue = values[index];

        keys[index]=key;
        values[index]=value;
        size++;
        return prevValue;
    }

    @Override
    public V remove(Object key) {
        Objects.requireNonNull(key);
        int index=search(key,keys,Objects::equals);
        if(index>-1) {
            V prevValue = values[index];

            keys[index] = null;
            values[index] = null;
            size--;

            return prevValue;
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends K,? extends V> m) {
        for (Map.Entry<? extends K,? extends V> entry : m.entrySet()) {
            this.put(entry.getKey(),entry.getValue());
        }


    }

    @Override
    public void clear() {
        for(int i=0;i<size;i++){
            keys[i]=null;
            values[i]=null;
        }
        size=0;

    }

    @Override
    public Set<K> keySet() {
        Set<K> resKeys = new HashSet<>();
        for(K i : keys)
            if (i != null)
                resKeys.add(i);
        return resKeys;
    }


    @Override
    public Collection<V> values() {
        Collection<V> resValues = new ArrayList<>();
        for(V i : values)
            if (i != null)
                resValues.add(i);
        return resValues;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> resSet = new HashSet<>();
        for(int i = 0; i < keys.length; ++i) {
            K key = keys[i];
            if (key != null) {
                V value = values[i];
                resSet.add(new AbstractMap.SimpleEntry<>(key, value));
            }
        }

        return resSet;
    }




    private <U> int search(U member, U[] array, BiPredicate<U, U> equalFunction){
        for(int i=0;i<array.length;i++)
            if(equalFunction.test(array[i],member))
                return i;
        return -1;
    }
    private int findEmpty(){
        return search(null, keys, Objects::equals);

    }

    private void expand(){
        int eSize=size*2;

        keys = Arrays.copyOf(keys, eSize);
        values = Arrays.copyOf(values, eSize);
    }



}
