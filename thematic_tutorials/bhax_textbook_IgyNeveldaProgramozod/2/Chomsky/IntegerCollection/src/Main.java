public class Main {
    public static void main(String[] args) {
        IntegerCollection numcollection=new IntegerCollection(7);
        int [] nums={101,10,23,2,50};
        numcollection.addToCollection(nums);

        numcollection.addToCollection(47);
        numcollection.addToCollection(21);

        System.out.print("Collection before sorting: ");
        numcollection.printCollection();
        numcollection.sort();
        System.out.print("Collection after sorting: ");
        numcollection.printCollection();

        System.out.println("\nContains 10: "+numcollection.contains(10));
        System.out.println("Contains 50: "+numcollection.contains(50));
        System.out.println("Contains 6: "+numcollection.contains(6));
        System.out.println("Contains 213: "+numcollection.contains(213));
    }
}
