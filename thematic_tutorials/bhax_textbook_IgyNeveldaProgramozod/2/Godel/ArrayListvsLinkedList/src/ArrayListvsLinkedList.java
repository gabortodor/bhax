import java.util.ArrayList;
import java.util.LinkedList;


public class ArrayListvsLinkedList {
    public static void main(String[] args) {
        ArrayList<Integer>a=new ArrayList<>();
        LinkedList<Integer>b=new LinkedList<>();

        for(int i=0;i<=10000;i++)
        {
            a.add(i);
            b.add(i);
        }
        System.out.println("\n ----get test----");
        Tests.getTime(a,1000);
        Tests.getTime(b,1000);
        System.out.println("\n ----remove test(high index)----");
        Tests.removeTime(a,5000);
        Tests.removeTime(b,5000);
        System.out.println("\n ----remove test(low index)----");
        Tests.removeTime(a,10);
        Tests.removeTime(b,10);

    }
}
