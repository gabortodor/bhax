public final class Pi{
    public static Pi instance;
    public double value;

    private Pi(double value){
        this.value=value;
    }

    public static Pi getInstance(double value){
        if(instance==null)
            instance=new Pi(value);
        return instance;
    }
}
