public interface AbstractFactory {
    Dog createDog();
    Chicken createChicken();
}
