public class BlackDog implements Dog {
    @Override
    public void makeSound() {
        System.out.println("WoofWoof");
    }

    @Override
    public void numberOfLegs() {
        System.out.println(4);
    }

    @Override
    public void getColor() {
        System.out.println("Black");

    }
}
