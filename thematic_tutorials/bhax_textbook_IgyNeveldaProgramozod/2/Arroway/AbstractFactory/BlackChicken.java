public class BlackChicken implements Chicken{
    @Override
    public void makeSound() {
        System.out.println("BwakBwak");
    }

    @Override
    public void numberOfLegs() {
        System.out.println("2");
    }

    @Override
    public void getColor() {
        System.out.println("Black");

    }
}
