public class Demo {
    private static Animal getAnimal(String type){
        AbstractFactory factory;
        switch(type){
            case"Black":
                factory=new BlackFactory();
                return new Animal(factory);
            case"White":
                factory=new WhiteFactory();
                return new Animal(factory);
            default:
                return null;
        }

    }
    public static void main(String[] args) {
       Animal animal=getAnimal("Black");
        animal.makeSound();
        animal.numberOfLegs();
        animal.getColor();
    }
}
