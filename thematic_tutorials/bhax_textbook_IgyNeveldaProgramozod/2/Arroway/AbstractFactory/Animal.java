public class Animal {
    private Dog dog;
    private Chicken chicken;
    public Animal(AbstractFactory factory){
        dog=factory.createDog();
        chicken=factory.createChicken();
    }
    public void makeSound(){
        dog.makeSound();
        chicken.makeSound();
    }
    public void numberOfLegs(){
        dog.numberOfLegs();;
        chicken.numberOfLegs();
    }
    public void getColor(){
        dog.getColor();
        chicken.getColor();
    }
}
