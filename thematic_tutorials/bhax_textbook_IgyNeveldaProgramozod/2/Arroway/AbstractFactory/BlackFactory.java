public class BlackFactory implements AbstractFactory{
    @Override
    public Dog createDog() {
        return new BlackDog();
    }

    @Override
    public Chicken createChicken() {
        return new BlackChicken();
    }
}
