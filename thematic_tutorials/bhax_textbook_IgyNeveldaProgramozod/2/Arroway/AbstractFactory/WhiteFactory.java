public class WhiteFactory implements AbstractFactory{
    @Override
    public Dog createDog() {
        return new WhiteDog();
    }

    @Override
    public Chicken createChicken() {
        return new WhiteChicken();
    }
}
