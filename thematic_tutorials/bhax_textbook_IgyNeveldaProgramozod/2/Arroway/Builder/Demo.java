public class Demo {
    public static void main(String[] args) {
        Director director=new Director();
        PCBuilder builder=new PCBuilder();
        director.buildHighEndPC(builder);
        PC highpc=builder.getResult();
        System.out.println("PC built:"+highpc.getType()+"\n");

        ManualBuilder manualbuilder=new ManualBuilder();
        director.buildHighEndPC(manualbuilder);
        Manual pcman=manualbuilder.getResult();
        System.out.println(pcman);
    }
}
