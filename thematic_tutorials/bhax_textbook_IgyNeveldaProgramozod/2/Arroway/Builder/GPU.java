public class GPU {
    private final String manufacturer;
    private final String model;
    private final int vram;

    public GPU(String manufacturer, String model, int vram) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.vram = vram;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public int getVram() {
        return vram;
    }

    @Override
    public String toString() {
        return manufacturer+" "+model+"("+vram+"GB)";
    }
}
