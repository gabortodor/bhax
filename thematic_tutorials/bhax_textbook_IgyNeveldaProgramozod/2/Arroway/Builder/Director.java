public class Director {

    public void buildHighEndPC(Builder builder){
        builder.setType(Type.HIGH_END_PC);
        builder.setCPU(new CPU("AMD","Ryzen 7 2700x",4.30,"AM4"));
        builder.setGPU(new GPU("Asus","RTX 2080 Ti",11));
        builder.setRAM(new RAM("Corsair","RGB Vengeance Pro",32,"DDR4",3600));
        builder.setHDD(new HDD("Seagate",4));
        builder.setSSD(new SSD("Kingston",500));
    }
    public void buildAveragePC(Builder builder){
        builder.setType(Type.AVERAGE_PC);
        builder.setCPU(new CPU("AMD","Ryzen 3 3100",3.6,"AM4"));
        builder.setGPU(new GPU("Gigabyte","GT1030",2));
        builder.setRAM(new RAM("Kingston","HyperX",8,"DDR3",1866));
        builder.setHDD(new HDD("Western Digital",1));
        builder.setSSD(null);
    }
    public void buildLowEndPC(Builder builder){
        builder.setType(Type.LOW_END_PC);
        builder.setCPU(new CPU("Intel","Pentium G5400",3.7,"Intel 1151 v2"));
        builder.setGPU(null);
        builder.setRAM(new RAM("CSX","CSXOD3LO13332GB",2,"DDR3",1333));
        builder.setHDD(new HDD("Toshiba",500));
        builder.setSSD(null);
    }
}
