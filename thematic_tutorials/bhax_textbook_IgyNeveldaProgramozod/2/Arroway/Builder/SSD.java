public class SSD {
    private final String manufacturer;
    private final int capacity;

    public SSD(String manufacturer, int capacity) {
        this.manufacturer = manufacturer;
        this.capacity = capacity;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return manufacturer+" "+capacity+"GB";
    }
}
