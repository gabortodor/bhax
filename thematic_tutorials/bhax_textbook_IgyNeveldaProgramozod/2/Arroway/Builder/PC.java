public class PC {
    private final Type type;
    private final CPU cpu;
    private final GPU gpu;
    private final RAM ram;
    private final HDD hdd;
    private final SSD ssd;

    public PC(Type type,CPU cpu,GPU gpu,RAM ram,HDD hdd,SSD ssd) {
        this.type=type;
        this.cpu=cpu;
        this.gpu=gpu;
        this.ram=ram;
        this.hdd=hdd;
        this.ssd=ssd;

    }

    public Type getType() {
        return type;
    }

    public CPU getCpu() {
        return cpu;
    }

    public GPU getGpu() {
        return gpu;
    }

    public RAM getRam() {
        return ram;
    }

    public HDD getHdd() {
        return hdd;
    }

    public SSD getSsd() {
        return ssd;
    }
}