public class ManualBuilder implements Builder{
    private Type type;
    private CPU cpu;
    private GPU gpu;
    private RAM ram;
    private HDD hdd;
    private SSD ssd;

    @Override
    public void setType(Type type) {
        this.type=type;
    }

    @Override
    public void setCPU(CPU cpu) {
        this.cpu=cpu;
    }

    @Override
    public void setGPU(GPU gpu) {
        this.gpu=gpu;
    }

    @Override
    public void setRAM(RAM ram) {
        this.ram=ram;
    }

    @Override
    public void setHDD(HDD hdd) {
        this.hdd=hdd;
    }

    @Override
    public void setSSD(SSD ssd) {
        this.ssd=ssd;
    }
    public Manual getResult() {
        return new Manual(type, cpu, gpu, ram, hdd, ssd);
    }
}