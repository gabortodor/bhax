public class CPU {
    private final String manufacturer;
    private final String model;
    private final double clockspeed;
    private final String socket;

    public CPU(String manufacturer,String model,double clockspeed,String socket){
        this.manufacturer=manufacturer;
        this.model=model;
        this.clockspeed=clockspeed;
        this.socket=socket;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public double getClockspeed() {
        return clockspeed;
    }

    public String getSocket() {
        return socket;
    }

    @Override
    public String toString() {
        return manufacturer+" "+model+"("+clockspeed+"GHz, "+socket+")";
    }
}
