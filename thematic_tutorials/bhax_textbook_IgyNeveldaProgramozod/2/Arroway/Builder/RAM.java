public class RAM {
    private final String manufacturer;
    private final String model;
    private final int capacity;
    private final String type;
    private final int speed;

    public RAM(String manufacturer,String model ,int capacity, String type, int speed) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.capacity = capacity;
        this.type = type;
        this.speed = speed;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getCapacity() {
        return capacity;
    }

    public String getType() {
        return type;
    }

    public int getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return manufacturer+" "+model+"("+capacity+"GB, "+type+", "+speed+"MHz)";
    }
}
