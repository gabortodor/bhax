public class Manual {
    private final Type type;
    private final CPU cpu;
    private final GPU gpu;
    private final RAM ram;
    private final HDD hdd;
    private final SSD ssd;

    public Manual(Type type,CPU cpu,GPU gpu,RAM ram,HDD hdd,SSD ssd) {
        this.type=type;
        this.cpu=cpu;
        this.gpu=gpu;
        this.ram=ram;
        this.hdd=hdd;
        this.ssd=ssd;
    }

    @Override
    public String toString() {
        String man="";
        man+="Computer Type: "+type+"\nCPU: "+cpu+"\nGPU: "+gpu+"\nRAM: "+ram+"\nHDD: "+hdd;
        if(this.ssd!=null)
            man+="\nSSD: "+ssd;
        return man;
    }
}
