public class HDD {
    private final String manufacturer;
    private final int capacity;

    public HDD(String manufacturer, int capacity) {
        this.manufacturer = manufacturer;
        this.capacity = capacity;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        if(capacity<=10)
            return manufacturer+" "+capacity+"TB";
        else
            return manufacturer+" "+capacity+"GB";
    }
}
