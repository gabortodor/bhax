public interface Builder {
    void setType(Type type);
    void setCPU(CPU cpu);
    void setGPU(GPU gpu);
    void setRAM(RAM ram);
    void setHDD(HDD hdd);
    void setSSD(SSD ssd);
}
