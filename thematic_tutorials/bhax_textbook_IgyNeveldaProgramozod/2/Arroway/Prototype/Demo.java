import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<RobotPrototype> workerclass=new ArrayList<>();
        List<RobotPrototype> farmerclass=new ArrayList<>();

        FactoryRobot facrobo=new FactoryRobot();
        facrobo.serialnumber=0;
        facrobo.cpu="RoboBrain 1010x";
        facrobo.height=250;
        facrobo.weight=360;
        facrobo.workplace="Car Factory";
        workerclass.add(facrobo);
        for(int i=0;i<100;i++)
            workerclass.add(workerclass.get(i).clone());

	System.out.println(workerclass.get(90));

        FarmerRobot farrobo=new FarmerRobot();
        farrobo.serialnumber=0;
        farrobo.cpu="RoboBrain 540t";
        farrobo.height=210;
        farrobo.weight=150;
        farrobo.croptype="corn";
        farmerclass.add(farrobo);
        for(int i=0;i<100;i++)
            farmerclass.add(workerclass.get(i).clone());

        System.out.println(farmerclass.get(56));
    }



}
