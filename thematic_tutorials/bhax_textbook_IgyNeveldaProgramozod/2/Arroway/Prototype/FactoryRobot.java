public class FactoryRobot extends RobotPrototype{
    public String workplace;

    public FactoryRobot(){
    }

    public FactoryRobot(FactoryRobot baserobot){
        super(baserobot);
        if(baserobot!=null)
            this.workplace=baserobot.workplace;
    }

    @Override
    public RobotPrototype clone() {
        return new FactoryRobot(this);
    }

    @Override
    public String toString() {
        return "FactoryRobot{" +
                ", serialnumber=" + serialnumber +
                ", cpu='" + cpu + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", workplace='" + workplace +
                '}';
    }
}
