public class FarmerRobot extends RobotPrototype{
    public String croptype;

    public FarmerRobot() {
    }
    public FarmerRobot(FarmerRobot baserobot){
        super(baserobot);
        if(baserobot!=null)
            this.croptype=baserobot.croptype;
    }

    @Override
    public RobotPrototype clone() {
        return new FarmerRobot(this);
    }

    @Override
    public String toString() {
        return "FarmerRobot{" +
                ", serialnumber=" + serialnumber +
                ", cpu='" + cpu + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", croptype='" + croptype +
                '}';
    }
}
