import java.util.Objects;

public abstract class RobotPrototype {
    public int serialnumber;
    public String cpu;
    public int height;
    public int weight;

    public RobotPrototype(){
    }

    public RobotPrototype(RobotPrototype baserobot){
        if(baserobot!=null)
            this.serialnumber=baserobot.serialnumber+1;
            this.cpu=baserobot.cpu;
            this.height=baserobot.height;
            this.weight=baserobot.weight;
    }

    public abstract RobotPrototype clone();

}
