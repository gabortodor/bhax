public class Twix implements Candy{

    @Override
    public void mainIngredients() {
        System.out.println("Twix:Milk Chocolate, Sugar, Enriched Wheat Flour, Palm Oil, Corn Syrup, Skim Milk, Dextrose, Artificial Flavor.");
    }

    @Override
    public void energy(int g) {
        System.out.println(Math.floor(g*5.02));
    }
}
