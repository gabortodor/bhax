public class Mars implements  Candy{
    @Override
    public void mainIngredients() {
        System.out.println("Mars:Sugar, Glucose Syrup, Milk Solids, Vegetable Fat, Cocoa Butter, Cocoa Mass, Barley Malt Extract, Cocoa Powder, Emulsifier, Salt, Egg White, Natural Flavours.");
    }

    @Override
    public void energy(int g) {
        System.out.println(Math.floor(g*4.5099));
    }
}
