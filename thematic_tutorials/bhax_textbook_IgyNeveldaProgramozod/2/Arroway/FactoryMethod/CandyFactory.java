public class CandyFactory {
    public Candy produceCandy(String type){
        switch(type){
            case "Snickers":
                return new Snickers();
            case "Twix":
                return new Twix();
            case "Mars":
                return new Mars();
            default:
                return null;
        }

    }
}
