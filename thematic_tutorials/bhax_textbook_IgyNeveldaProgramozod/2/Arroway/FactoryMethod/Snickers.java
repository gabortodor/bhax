public class Snickers implements Candy{
    @Override
    public void mainIngredients() {
        System.out.println("Snickers:Milk chocolate, Peanuts, Corn Syrup, Sugar, Palm Oil, Skim Milk, Lactose, Salt, Egg Whites, Talia, Artificial Flavor.");
    }

    @Override
    public void energy(int g) {
        System.out.println(Math.floor(g*4.6809));
    }
}
