public class Demo {
    public static void main(String[] args) {
        CandyFactory factory=new CandyFactory();
        Candy a=factory.produceCandy("Snickers");
        Candy b=factory.produceCandy("Twix");
        Candy c=factory.produceCandy("Mars");
        a.mainIngredients();
        a.energy(47);
        b.mainIngredients();
        b.energy(100);
        c.mainIngredients();
        c.energy(89);
    }
}
