#include <iostream>

class Person {
	private:
		std::string name;
	public:
		std::string getName() {
			return name;
		}

		std::string setName(std::string name) {
			this->name = name;
		}
};

class Student:public Person {
private:
	std::string university;
public:
	std::string getUniversity() {
		return university;
	}

	std::string setUniversity(std::string university) {
		this->university = university;
	}
};

int main()
{
	Student stud1;
	stud1.setName("John");
	stud1.setUniversity("University of Debrecen");

    std::cout << "Name:"<<stud1.getName()<<",University:"<<stud1.getUniversity();

	Person stud2;
	stud2.setName("Amy");
	stud2.setUniversity("University of Debrecen");

	std::cout << "Name:" << stud2.getName() << ",University:" << stud2.getUniversity();
}
