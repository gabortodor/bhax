public abstract class Student {

    private String name;
    private String neptunid;
    private final String university="University of Debrecen";
    private String course;

    public Student(String nm, String id, String crs){
        this.name=nm;
        this.neptunid=id;
        this.course=crs;
    }

    public abstract void learn();

    @Override
    public String toString(){
        return "Name="+this.name+"::Neptunid="+this.neptunid+"::University="+this.university+"::Course="+this.course;
    }

    public void changeCourse(String newCourse) {
        this.course = newCourse;
    }
}
