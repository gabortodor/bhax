public class Dog implements Animal{
    @Override
    public void makeSound() {
        System.out.println("WoofWoofWoof");
    }

    @Override
    public void numberOfLegs() {
        System.out.printf("I have 4 legs");
    }
}
