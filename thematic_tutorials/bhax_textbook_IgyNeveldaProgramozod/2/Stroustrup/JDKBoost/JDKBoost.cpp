#include <iostream>
#include <boost/filesystem.hpp>


int main(int argc, char* argv[])
{
    int num=0;
    std::string javaext (".java");


    for(boost::filesystem::recursive_directory_iterator i=boost::filesystem::recursive_directory_iterator ("."); i!=boost::filesystem::recursive_directory_iterator(); i++)
    {
      
      if (boost::filesystem::extension(*i)==javaext)
      {
        num++;
        std::cout<<num<<".java class:"<<boost::filesystem::path(*i).stem().string()<<".java"<<std::endl;
      }
    }

    std::cout<<std::endl<<"Number of Java classes in JDK source: "<<num<<std::endl;
}

