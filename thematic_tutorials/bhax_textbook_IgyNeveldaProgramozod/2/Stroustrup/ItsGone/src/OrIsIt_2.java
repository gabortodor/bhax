import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class OrIsIt_2 {

    public static void main(String[] args) throws Exception {
        try (BugousProducer producer = new BugousProducer("output.txt")) {
            producer.writeStuff();
        }
    }

    private static class BugousProducer implements AutoCloseable {
        private final Writer writer;

        public BugousProducer(String outputFileName) throws IOException {
            writer = new FileWriter(outputFileName);
        }

        public void writeStuff() throws IOException {
            writer.write("Stuff");
        }

        @Override
        public void close() throws Exception {
            writer.close();
        }
    }
}
