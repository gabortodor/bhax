import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class ItsGone {

    public static void main(String[] args) throws IOException {
        BugousProducer producer = new BugousProducer("output.txt");
        producer.writeStuff();
    }

    private static class BugousProducer {
        private final Writer writer;

        public BugousProducer(String outputFileName) throws IOException {
            writer = new FileWriter(outputFileName);
        }

        public void writeStuff() throws IOException {
            writer.write("Stuff");
        }

        @Override
        public void finalize() throws IOException {
            writer.close();
        }
    }
}