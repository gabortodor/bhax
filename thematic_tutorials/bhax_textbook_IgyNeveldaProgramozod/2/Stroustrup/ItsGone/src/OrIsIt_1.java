import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class OrIsIt_1 {

    public static void main(String[] args) throws Exception {
        BugousProducer producer = new BugousProducer("output.txt");
        try{
            producer.writeStuff();
        }
        finally {
            producer.finalize();
        }
    }

    private static class BugousProducer {
        private final Writer writer;

        public BugousProducer(String outputFileName) throws IOException {
            writer = new FileWriter(outputFileName);
        }

        public void writeStuff() throws IOException {
            writer.write("Stuff");
        }

        @Override
        public void finalize() throws IOException {
            writer.close();
        }
    }
}
