import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class IO implements AutoCloseable {
    private Scanner scanner=new Scanner(System.in);

    public void inputReader(int offset,String filename) {
        StringBuilder sb = new StringBuilder();
        CaesarCoder cc=new CaesarCoder(offset);
        String text;
        do {
            text = scanner.nextLine();
            sb.append(cc.encoding(text));
        } while (!"q".equals(text));
        String encryptedText=sb.toString();
        writer(encryptedText.substring(0, encryptedText.length()-3),filename);
    }

    public void writer(String text,String filename)  {
        try( FileWriter writer=new FileWriter(filename)) {
            writer.append(text);
        }
        catch (IOException e){
            System.out.println(e);
        }
        System.out.println("Encryption successful!");
    }

    @Override
    public void close() throws Exception {
    }
}

