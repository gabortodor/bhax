public class Main {
    public static void main(String[] args){

        if (args.length==2) {
            int offset;
            try {
                offset = Integer.parseInt(args[0]);
            }
            catch(NumberFormatException e){
                System.out.println("Error:The offset parameter has to be an Integer!");
                return;
            }
            String filename=args[1];
            IO io=new IO();
            io.inputReader(offset,filename);
        }
        else
            System.out.println("Needed parameters: offset, text");
    }
}
