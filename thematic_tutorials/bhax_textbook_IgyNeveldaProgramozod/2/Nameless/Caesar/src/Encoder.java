public interface Encoder {
    String encoding(String text);
}
