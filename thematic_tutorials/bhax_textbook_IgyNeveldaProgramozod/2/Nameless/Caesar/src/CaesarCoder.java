public class CaesarCoder implements Encoder{
    int offset;

    public CaesarCoder(int offset) {
        this.offset = offset;
    }

    @Override
    public String encoding(String text) {
        StringBuilder sb= new StringBuilder();
        for(char c:text.toCharArray())
        {
            if(c>='A'&&c<='Z')
                sb.append((char)((c - 'A' + offset) % 26 + 'A'));
            else if(c>='a'&&c<='z')
                sb.append((char)((c - 'a' + offset) % 26 + 'a'));
            else
                sb.append(c);
        }
        return sb.toString()+"\n";
    }
}
